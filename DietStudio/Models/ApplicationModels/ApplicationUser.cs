﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DietStudio.Models.ApplicationModels
{
	public class ApplicationUser : IdentityUser
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Family { get; set; }

		public string PhotoPath { get; set; }

		public int Age { get; set; }


		public int SexId { get; set; }
		[ForeignKey("SexId")]
		public Sex Sex { get; set; }

		public ICollection<Client> Clients { get; set; }

		public ApplicationUser() { }
		public ApplicationUser(ApplicationUser client) 
		{
			Id = client.Id;
			Age = client.Age;
			SexId = client.SexId;
			Sex = client.Sex;
			FirstName = client.FirstName;
			LastName = client.LastName;
			Family = client.Family;
			PhotoPath = client.PhotoPath;
			Clients = new List<Client>();
		}

		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
		{
			// Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
			var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
			// Add custom user claims here
			return userIdentity;
		}
	}
}