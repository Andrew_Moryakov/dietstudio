﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DietStudio.Models.ApplicationModels
{

	public class Purpose
	{
		[Key]
		public int Id { get; set; }

		public string PurposeTitle { get; set; }

		public Purpose()
		{
		}

		public Purpose(int id, string purposeTitle)
		{
			Id = id;
			PurposeTitle = purposeTitle;
		}
	}

	public class Sex
	{
		[Key]
		public int Id { get; set; }

		public string SexTitle { get; set; }
	}

	#region Client
	[NotMapped]
	public class Client
	{
		[Key]
		public int Id { get; set; }

		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Family { get; set; }

		public double Height { get; set; }

		public double Weight { get; set; }

		public string PhotoPath { get; set; }

		[NotMapped]
		public double Pfc { get; set; }

		[NotMapped]
		public double TargetPfc { get; set; }

		[NotMapped]
		public double TargetP { get; set; }

		[NotMapped]
		public double TargetF { get; set; }

		[NotMapped]
		public double TargetC { get; set; }

		public int Age { get; set; }

		public int? PurposeForClientId { get; set; }
		[ForeignKey("PurposeForClientId")]
		public PurposeForClient PurposeForClient { get; set; }

		public int? ApplicationUserId { get; set; }
		[ForeignKey("ApplicationUserId")]
		public ApplicationUser ApplicationUser { get; set; }

		public int SexId { get; set; }
		[ForeignKey("SexId")]
		public Sex Sex { get; set; }

		public int? MainDietId { get; set; }
		[ForeignKey("MainDietId")]
		public Diet MainDiet { get; set; }

		public ICollection<CategoryProcent> ProcentFoodCategories { get; set; }
		public ICollection<DateTimeDiet> Diets { get; set; }
		public ICollection<FoodIntakeForClient> FoodIntakes { get; set; }

		public Client()
		{
			Diets = new List<DateTimeDiet>();
			ProcentFoodCategories = new List<CategoryProcent>();
			FoodIntakes = new List<FoodIntakeForClient>();
		}

		public Client(Client client) : this()
		{
			Id = client.Id;
			Age = client.Age;
			PurposeForClientId = client.PurposeForClientId;
			PurposeForClient = client.PurposeForClient;
			SexId = client.SexId;
			Sex = client.Sex;
			MainDietId = client.MainDietId;
			FirstName = client.FirstName;
			LastName = client.LastName;
			Family = client.Family;
			Height = client.Height;
			Weight = client.Weight;
			PhotoPath = client.PhotoPath;
		}
	}
	#endregion

	public class FoodIntakeForClient
	{
		[Key]
		public int Id { get; set; }

		public string ClientId { get; set; }
		[ForeignKey("ClientId")]
		public Client Client { get; set; }

		public int FoodIntakesId { get; set; }
		[ForeignKey("FoodIntakesId")]
		public FoodIntake FoodIntake { get; set; }

		public DateTime Time { get; set; }
	}

	public class PurposeForClient
	{
		[Key]
		public int Id { get; set; }

		//public string ClientId { get; set; }
		//[ForeignKey("ClientId")]
		//public Client Client { get; set; }

		public int PurposeId { get; set; }
		[ForeignKey("PurposeId")]
		public Purpose Purpose { get; set; }

		public double PurposeСoefficient { get; set; }

		////	[InverseProperty("Client")] // <- Navigation property name in EntityA
		//public ICollection<Client> Clients { get; set; }

		//public PurposeForClient()
		//{
		//	Clients = new List<Client>();
		//}
	}

	public class DateTimeDiet
	{
		[Key]
		public int Id { get; set; }
		public Diet Diet { get; set; }
		public DateTime DateTime { get; set; }
	}

	public class Recipe
	{
		[Key]
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public ICollection<FoodProductForRecipe> Foods { get; set; }

		public Recipe()
		{
			Foods = new List<FoodProductForRecipe>();
		}

		public Recipe(string name, string description, ICollection<FoodProductForRecipe> foods)
		{
			Name = name;
			Description = description;
			Foods = foods;
		}
	}

	public class FoodProductForRecipe
	{
		[Key]
		public int Id { get; set; }

		public double Weight { get; set; }

		public string Description { get; set; }

		public int FoodProductId { get; set; }
		[ForeignKey("FoodProductId")]
		public FoodProduct Product { get; set; }

		public int MeasuringId { get; set; }
		[ForeignKey("MeasuringId")]
		public Measuring Measuring { get; set; }

		public int RecipeId { get; set; }
		[ForeignKey("RecipeId")]
		public Recipe Recipe { get; set; }

		public FoodProductForRecipe(){}

		public FoodProductForRecipe(double weight, string description, FoodProduct product, Measuring measuring)
		{
			Weight = weight;
			Description = description;
			Product = product;
			Measuring = measuring;
		}
	}

	public class FoodProduct
	{
		public FoodProduct()
		{
			Diets = new List<Diet>();
		}

		public FoodProduct(FoodProduct fp) : this()
		{
			Id = fp.Id;
			Diets = fp.Diets;
			Name = fp.Name;
			Protein = fp.Protein;
			Fats = fp.Fats;
			Carbohydrates = fp.Carbohydrates;
			CategoryId = fp.Category?.Id;
			Weight = fp.Weight;
			MeasuringId = fp.MeasuringId;
		}

		public FoodProduct(string name, double protein, double fats, double carbohydrates, double weight, Measuring m, Category category = null) : this()
		{
			Name = name;
			Protein = protein;
			Fats = fats;
			Carbohydrates = carbohydrates;
			CategoryId = category?.Id;
			Weight = weight;
			MeasuringId = m.Id;
		}

		[Key]
		public int Id { get; set; }
		public string Name { get; set; }
		public List<string> Labels { get; set; }
		public double Protein { get; set; }
		public double Fats { get; set; }
		public double Carbohydrates { get; set; }
		public double Weight { get; set; }
		public ICollection<Diet> Diets { get; set; }

		public int? CategoryId { get; set; }
		[ForeignKey("CategoryId")]
		public Category Category { get; set; }

		public int? MeasuringId { get; set; }
		[ForeignKey("MeasuringId")]
		public Measuring Measuring { get; set; }

		public int? CurrencyId { get; set; }
		[ForeignKey("CurrencyId")]
		public Currency Currency { get; set; }
	}

	public class Measuring
	{
		[Key]
		public int Id { get; set; }
		public ICollection<FoodProduct> Products { get; set; }
		public string Name { get; set; }

		public Measuring(string name) :this()
		{
			Name = name;
		}

		public Measuring()
		{
			Products = new List<FoodProduct>();
		}
	}

	public class Currency
	{
		[Key]
		public int Id { get; set; }
		public ICollection<FoodProduct> Products { get; set; }
		public string Name { get; set; }

		public Currency(string name) : this()
		{
			Name = name;
		}

		public Currency()
		{
			Products = new List<FoodProduct>();
		}
	}

	public class Category
	{
		[Key]
		public int Id { get; set; }

		public string Name { get; set; }

		public ICollection<FoodProduct> Products { get; set; }

		public Category(string name) : this()
		{
			Name = name;
		}

		public Category()
		{
			Products = new List<FoodProduct>();
		}
	}

	public class BanedProduct
	{
		[Key]
		public int Id { get; set; }

		public int FoodProductId { get; set; }
		[ForeignKey("FoodProductId")]
		public FoodProduct Product { get; set; }

		public int FoodIntakesId { get; set; }
		[ForeignKey("FoodIntakesId")]
		public FoodIntake FoodIntakes { get; set; }
	}

	public class Diet
	{
		[Key]
		public int Id { get; set; }

		public string Name { get; set; }
		public double Weight { get; set; }
		public ICollection<Client> Clients { get; set; }
		public ICollection<DietComponent> DietComponents { get; set; }

		public Diet(string name) : this(name, null)
		{
			Name = name;
		}

		public Diet(string name, List<DietComponent> dietComponents ) :this()
		{
			Name = name;
			DietComponents = dietComponents;
		}

		public Diet()
		{
			DietComponents = new List<DietComponent>();
			Clients = new List<Client>();
		}
	}

	public class DietComponent
	{
		public DietComponent()
		{
		}
		public DietComponent(FoodIntake foodIntake, FoodProduct foodProduct, double weight, Diet diet) : this(foodIntake, foodProduct, weight)
		{
			Diet = diet;
		}

		public DietComponent(FoodIntake foodIntake, FoodProduct foodProduct, double weight)
		{
			FoodIntakesId = foodIntake.Id;
			ProductId = foodProduct.Id;
			Weight = weight;
		}

		[Key]
		public int Id { get; set; }

		public int ProductId { get; set; }
		[ForeignKey("ProductId")]
		public FoodProduct Product { get; set; }

		public int FoodIntakesId { get; set; }
		[ForeignKey("FoodIntakesId")]
		public FoodIntake FoodIntakes { get; set; }

		public int DietId { get; set; }
		[ForeignKey("DietId")]
		public Diet Diet { get; set; }

		public double Weight { get; set; }
	}

	[NotMapped]
	public class FoodIntakeProductList
	{
		public FoodIntakeProductList()
		{
			ProductList = new ObservableCollection<FoodProduct>();
		}

		public FoodIntakeProductList(FoodIntake foodIntakes, ObservableCollection<FoodProduct> products):this()
		{
			FoodIntakes = foodIntakes;
			ProductList = products;
		}

		[Key]
		public int Id { get; set; }

		public FoodIntake FoodIntakes { get; set; }

		private ObservableCollection<FoodProduct> _foods;
		public ObservableCollection<FoodProduct> ProductList { get; set; }
	}

	public class CategoryProcent
	{
		[Key]
		public int Id { get; set; }

		public int Procent { get; set; }

		public int CategoryId { get; set; }
		[ForeignKey("CategoryId")]
		public Category Category { get; set; }

		public string ClientId { get; set; }
		[ForeignKey("ClientId")]
		public Client Clients { get; set; }
	}

	public class FoodIntake
	{
		public FoodIntake()
		{
			
		}

		public FoodIntake(string name, int number)
		{
			Name = name;
			Number = number;
			Clients = new List<Client>();
		}

		[Key]
		public int Id { get; set; }

		public ICollection<Client> Clients { get; set; }
		public string Name { get; set; }
		public int Number { get; set; }
	}

	public class BanedCross
	{
		[Key]
		public int Id { get; set; }

		public FoodProduct FoodProductOneId { get; set; }
		public FoodProduct FoodProductTwoId { get; set; }
	}
}