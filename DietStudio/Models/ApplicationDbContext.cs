﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using DietStudio.App_Start;
using DietStudio.Models.ApplicationModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DietStudio.Models
{
	public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
	{

		public ApplicationDbContext()
			: base("DefaultConnection", throwIfV1Schema: false)
		{ 
		}

		//public DbSet<ApplicationUser> ApplicationUsers { get; set; }
		public DbSet<Sex> Sexes { get; set; }
		public DbSet<Purpose> Purposes { get; set; }
		//public DbSet<DietName> DietNames { get; set; }
		public DbSet<FoodProduct> FoodProducts { get; set; }
		public DbSet<Diet> Diets { get; set; }
		public DbSet<Category> Categories { get; set; }
		public DbSet<CategoryProcent> ClientProcents { get; set; }
		public DbSet<FoodIntake> FoodIntakes { get; set; }
		public DbSet<BanedCross> BanedCrosses { get; set; }
		public DbSet<BanedProduct> BanedProducts { get; set; }
		public DbSet<DietComponent> DietComponents { get; set; }
		public DbSet<DateTimeDiet> DateTimeDiets { get; set; }
		public DbSet<Measuring> Measurings { get; set; }
		public DbSet<Recipe> Recipes { get; set; }

		public static ApplicationDbContext Create()
		{
			return new ApplicationDbContext();
		}

		public System.Data.Entity.DbSet<DietStudio.Models.ApplicationModels.PurposeForClient> PurposeForClients { get; set; }
	}

	public class AppDbInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
	{
		protected override void Seed(ApplicationDbContext context)
		{
			List<Sex> sexes = new List<Sex>
			{
				new Sex
				{
					SexTitle = "Женский"
				},
				new Sex
				{
					SexTitle = "Мужской"
				}
			};

			List<Purpose> purpose = new List<Purpose>
			{
				new Purpose
				{
					PurposeTitle = "Похудение"
				},
				new Purpose
				{
					PurposeTitle = "Набор"
				},
				new Purpose
				{
					PurposeTitle = "Тонус"
				}
			};
			List<FoodIntake> foodIntakes = new List<FoodIntake>
			{
				new FoodIntake("Завтрак", 1),
				new FoodIntake("Обед", 2),
				new FoodIntake("Ужин", 3)
			};
			List<Measuring> measurings = new List<Measuring>
			{
				new Measuring("Грамм"),
			new Measuring("Литр")
			};

			//if (!context.Roles.Any(r => r.Name == "Admin"))
			//{
			//	var store = new RoleStore<IdentityRole>(context);
			//	var manager = new RoleManager<IdentityRole>(store);
			//	var role = new IdentityRole { Name = "Admin" };

			//	manager.Create(role);
			//}

			//if (!context.Users.Any(u => u.UserName == "Administrator"))
			//{
			//	var store = new UserStore<ApplicationUser>(context);
			//	var manager = new UserManager<ApplicationUser>(store);
			//	var user = new ApplicationUser
			//	{
			//		UserName = "Administrator",
			//		Email = "admin@admin.com"
			//	};

			//	manager.Create(user, "admin");

			//	manager.AddToRole(user.Id, "Admin");
			//}


			context.FoodIntakes.AddRange(foodIntakes);
			context.Sexes.AddRange(sexes);
			context.Purposes.AddRange(purpose);
			context.Measurings.AddRange(measurings);

				context.SaveChanges();

			base.Seed(context);
		}


		public void AddUser(ApplicationDbContext context)
		{
			//if (adminInDb == null)
			//{
			var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
			var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

			var role = new IdentityRole
			{
				Name = "admin"
			};
			var roleEmployee = new IdentityRole
			{
				Name = "employee"
			};
			var roleUser = new IdentityRole
			{
				Name = "user"
			};

			roleManager.Create(role);
			roleManager.Create(roleEmployee);
			roleManager.Create(roleUser);

			var adminUser = new ApplicationUser
			{
				Email = "admin@admin.com",
				UserName = "admin"
			};
			const string password = "admin";
			var result = userManager.Create(adminUser, password);

			if (result.Succeeded)
			{
				userManager.AddToRole(adminUser.Id, role.Name);
			}
		}
	}
}