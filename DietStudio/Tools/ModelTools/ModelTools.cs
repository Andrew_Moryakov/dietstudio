﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using DietStudio.Models;

namespace DietStudio.Tools.ModelTools
{
	public class ModelTools<T> where T : class //, IModel
	{
		/// <returns>Список клиентов</returns>
		public static async Task<T> MakeAsync(T entity, EntityState state)
		{
			return await Task.FromResult(Make(entity, state));
		}

		/// <summary>
		/// Производит действие над сущностью, на основе переданного состояния.
		/// </summary>
		/// <param name="entity">сущность, которую нужно добавить</param>
		/// <param name="state">Состояние, характеризующие действие над сущностью</param>
		/// <returns>Возвращает объект, который был созданна</returns>
		/// <exception cref="DbEntityValidationException">
		///             The save was aborted because validation of entity property values failed.
		///             </exception>
		/// <exception cref="DbUpdateConcurrencyException">
		///             A database command did not affect the expected number of rows. This usually indicates an optimistic 
		///             concurrency violation; that is, a row has been changed in the database since it was queried.
		///             </exception>
		/// <exception cref="DbUpdateException">An error occurred sending updates to the database.</exception>
		/// <exception cref="InvalidOperationException">
		///             Some error occurred attempting to process entities in the context either before or after sending commands
		///             to the database.
		///             </exception>
		/// <exception cref="ObjectDisposedException">The context or connection have been disposed.</exception>
		/// <exception cref="NotSupportedException">
		///             An attempt was made to use unsupported behavior such as executing multiple asynchronous commands concurrently
		///             on the same context instance.</exception>
		public static T Make(T entity, EntityState state)
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				db.Entry(entity).State = state;
				db.SaveChanges();

				return entity;
			}
		}

		/// <summary>
		/// Возвращает сущности из базы данных, с подключением указанных зависимостей.
		/// </summary>
		/// <param name="includes">Зависимости.</param>
		/// <returns>Все элементы из базы данных</returns>
		public static IQueryable<T> Get(params string[] includes)
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				IQueryable<T> entityQuery = db.Set<T>();

				foreach (var include in includes)
					entityQuery = entityQuery.Include(include);

				return entityQuery;
			}
		}
	}
}