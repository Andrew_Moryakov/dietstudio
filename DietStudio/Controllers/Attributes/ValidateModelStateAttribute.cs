﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace DietStudio.Controllers.Attributes
{
	/// <summary>
	/// Атрибут проверяет корректность данных
	/// </summary>
	public class ValidateModelStateAttribute : ActionFilterAttribute
	{
		/// <summary>
		/// Конструктор устанавливает пользовательский метод проверки
		/// </summary>
		/// <param name="type">Тип, в котором расположен метод, осуществляющий проверку вхоящих данных</param>
		/// <param name="func">Метод, который осуществляет проверку входящих данных. Если данные корректные метод должен возвращать истин</param>
		public ValidateModelStateAttribute(Type type, string func)
		{
			try
			{
				_methodValidate = type.GetMethod(func);
			}
			catch
			{
				_methodValidate = null;
			}
		}

		/// <summary>
		/// Конструктор по умолчанию
		/// </summary>
		public ValidateModelStateAttribute()
		{

		}

		/// <summary>
		/// Инкапсулирует метод пользовательской проверки данных
		/// </summary>
		private MethodInfo _methodValidate;

		private bool CustomValidation(HttpActionContext actionContext)
		{
			bool isValid = true;
			if (actionContext.ActionArguments.Any() && _methodValidate != null)
			{
				dynamic objectValue = actionContext.ActionArguments.First().Value;
				isValid = (bool)_methodValidate.Invoke(null, new object[1] { objectValue });
			}
			return isValid;
		}

		public override void OnActionExecuting(HttpActionContext actionContext)
		{
			if (!actionContext.ModelState.IsValid)
			{
				actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest,
						actionContext.ModelState);
			}
			else
			if (!CustomValidation(actionContext))
			{
				actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.NotFound, new HttpError());
			}
		}
	}
}